@extends('adminlte.master')

@section('content')
    <div class="mt-5 ml-5">
        <h4> {{ $post->title }}</h4>
        <p> {{ $post->body }}</p>
        <p> Author : {{ $post->author->name}}</p>
        <div>
            Tags :
            @forelse($post->tags as $tag)
                <button class="btn btn-primary btn-sm">
                    {{ $tag->tag_name }}<!-- mewakili model dari tag-->
                </button>
                @empty
                    No Tags
            @endforelse
        </div>
    </div>
@endsection