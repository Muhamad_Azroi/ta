@extends('adminlte.master')

@push('headScript')
  <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush

@section('content')
<div class="ml-3 mt-3">
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Enter new film</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="/posts" method="POST" enctype="multipart/form-data">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <div class="custom-file">    
              <input type="file" class="custom-file-input" name="image" value="">
              <label for="image" class="custom-file-label">Image</label>
              @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
              @enderror
          </div>
       </div>
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" value="{{ old('title', '') }}"placeholder="Enter Title" required>
          @error('title') <!-- validasi frontend via bootstrap -->
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror          
        </div>
        <div class="form-group">
          <label for="body">Body</label>
          <textarea name="body" class="form-control my-editor">{{old('body','')}}</textarea>
          @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
          @enderror
        </div>
        
        <div class="form-group">
          <label for="tags">Tags</label>
          <input type="text" name="tags" id="tags" class="form-control" placeholder="Pisahkan dengan koma, contoh: postingan,berita,update" value="{{ old('tags', '') }} ">
        </div>


      </div>
      <!-- /.card-body -->
      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Create</button>
      </div>
    </form>
  </div>
</div>
@endsection

@push('bodyScript')
  <script>
    var editor_config = {
      path_absolute : "/",
      selector: "textarea.my-editor",
      plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern"
      ],
      toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
      relative_urls: false,
      file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
          cmsURL = cmsURL + "&type=Images";
        } else {
          cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
          file : cmsURL,
          title : 'Filemanager',
          width : x * 0.8,
          height : y * 0.8,
          resizable : "yes",
          close_previous : "no"
        });
      }
    };

    tinymce.init(editor_config);
  </script>
@endpush