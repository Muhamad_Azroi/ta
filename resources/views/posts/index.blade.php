@extends('adminlte.master')

@section('content')
     <div class="ml-3 mt-3">
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Posts Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @if(session('success'))
                    <!-- Menampilkan pesan success setelah create-->
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                <a href="{{ route('posts.create') }}" class="btn btn-primary mb-2">Create New Post</a>

                <table class="table table-bordered">
                    <thead>                  
                    <tr>
                        <th style="width: 10px">No.</th>
                        <th>Images</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($posts as $key => $post) <!-- untuk setiap post di dalam array posts kita ambil keynya-->
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td><img src="{{asset('/storage/image/'.$post->image)}}" style="width: 50%"></td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->body }}</td>   
                        <td class="d-flex align-self-stretch">
                            <a href="{{ route('posts.show', ['post' => $post->id]) }}" class="btn btn-info btn-sm">Show</a>
                            <a href="/posts/{{$post->id}}/download" class="btn btn-success btn-sm">Download</a>
                            <a href="/posts/{{$post->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                            <form action="/posts/{{$post->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center">No Posts</td> <!-- Jika tidak diberi ini keluar tabel-->
                    </tr>
                    @endforelse
                    </tbody>
                </table>
                </div>
                <!-- /.card-body -->
                {{-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
                </div> --}}
          </div>
     </div>
@endsection