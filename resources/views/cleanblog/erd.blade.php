<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Curahan Kepala</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset("/cleanblog/vendor/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset("/cleanblog/vendor/fontawesome-free/css/all.min.css") }}" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="{{ asset("/cleanblog/css/clean-blog.min.css")}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  @include('cleanblog.partial.navbar')

  <!-- Page Header -->
  @include('cleanblog.partial.header')

  <!-- Main Content -->
  <div class="container">
    <div class="row">
      
        <img src="{{ asset('/img/blog_erd.png')}}" alt="" srcset="">
      
    </div>
  </div>

  <hr>

  <!-- Footer -->
  <footer>
    @include('cleanblog.partial.footer')
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset("/cleanblog/vendor/jquery/jquery.min.js")}}"></script>
  <script src="{{ asset("/cleanblog/vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset("/cleanblog/js/clean-blog.min.js")}}"></script>

</body>

</html>
