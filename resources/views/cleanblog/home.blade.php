@extends('cleanblog.master')

@section('content')
    
    <div class="col-lg-8 col-md-10 mx-auto">
    @forelse($post as $item)    
        <div class="post-preview">
        <a href="/home/{{$item->id}}">
            <h2 class="post-title">
            {{ $item->title}}
            </h2>
            <h3 class="post-subtitle">
            Problems look mighty small from 150 miles up
            </h3>
        </a>
        <p class="post-meta">Posted by
            <a href="#">{{$item->author->name}}</a>
            on {{ $item->created_at}}</p>
        </div>
        <hr>
        @empty 
            Belum ada postingan, bergabunglah bersama kami dan berkonstribusi
    @endforelse

@endsection