<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset("/cleanblog/vendor/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="{{ asset("/cleanblog/vendor/fontawesome-free/css/all.min.css") }}" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="{{ asset("/cleanblog/css/clean-blog.min.css")}}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  @include('cleanblog.partial.navbar')

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('{{ asset("/cleanblog/img/home-bg.jpg")}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>{{ $post->title }}</h1>
            <span class="subheading">Posted by
                <a href="#">{{ $post->author->name}}</a>
                on {{ $post->created_at}}</p></span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <!-- Main Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <img src="{{asset('/storage/image/'.$post->image)}}" >
        </div>
      </div>
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          {!! $post->body !!}

          <p>Placeholder text by
            <a href="http://spaceipsum.com/">Space Ipsum</a>. Photographs by
            <a href="https://www.flickr.com/photos/nasacommons/">NASA on The Commons</a>.</p>
        </div>
      </div>
    </div>
  </article>

  <!-- Footer -->
  <footer>
    @include('cleanblog.partial.footer')
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset("/cleanblog/vendor/jquery/jquery.min.js")}}"></script>
  <script src="{{ asset("/cleanblog/vendor/bootstrap/js/bootstrap.bundle.min.js")}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{ asset("/cleanblog/js/clean-blog.min.js")}}"></script>

</body>

</html>
