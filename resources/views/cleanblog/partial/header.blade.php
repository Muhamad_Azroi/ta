<header class="masthead" style="background-image: url('{{ asset("/cleanblog/img/home-bg.jpg")}}')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Curahan Kepala</h1>
            <span class="subheading">Sebuah Wadah Terbuka bagi Penulis untuk Mencurahkan Isi Kepala Mereka Demi Meningkatkan Literasi Bangsa</span>
          </div>
        </div>
      </div>
    </div>
  </header>