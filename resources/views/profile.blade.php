@extends('adminlte.master')
@section('profile')

@section('content')


           
            <div class="card card-widget widget-user">
             
              <div class="widget-user-header     text-white" style="background: url({{asset('/adminlte/dist/img/photo1.png')}}) center center;">
                <h3 class="widget-user-username text-right">{{ Auth::user()->name }}</h3>
                <h5 class="widget-user-desc text-right">Web Designer</h5>
              </div>
              <div class="widget-user-image">
                <img class="img-circle" src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" alt="User Avatar">
              </div>
              <div class="card-footer">
                <div class="row">
                  
                  <div class="col-sm-12">
                    <div class="description-block">
                      <b><span class="description-text">{{ Auth::user()->name }}</span></b> 
                    </div>

                  </div>
                  
                  
                </div>
              
              </div>
            </div>

          
            <div class="float-right ml-2 mr-3 mt-4">
                <a class="btn btn-secondary" href="{{ route('posts.index') }}"> Create</a>
            </div>
    

            <div class="float-right mr-3 ml-2 mt-4">
                <a class="btn btn-secondary" href="{{ route('posts.index') }}"> Update</a>
            </div>
            <br>
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Profil</h3>
              </div>
    
              <div class="card-body">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td style="width: 3%">1.</td>
                      <td>Nama</td>
                      <td style="width: 55%">
                      @foreach ($profile as $object)
                        {{ $object->nama }}
                      @endforeach
                      </td>
                      
                    </tr>
                    <tr>
                      <td>2.</td>
                      <td>Alamat</td>
                      <td>
                      @foreach ($profile as $object)
                        {{ $object->alamat }}
                      @endforeach
                      </td>
                      
                    </tr>
                    <tr>
                      <td>3.</td>
                      <td>Jenis Kelamain</td>
                      <td>
                      @foreach ($profile as $object)
                        {{ $object->kelamin }}
                      @endforeach
                      </td>
                      
                    </tr>
                    <tr>
                      <td>4.</td>
                      <td>Kewarganeraan</td>
                      <td>
                      @foreach ($profile as $object)
                        {{ $object->kewarganegaraan}}
                      @endforeach
                      </td>
                      
                    </tr>
                    <tr>
                      <td>5.</td>
                      <td>Agama</td>
                      <td>
                      @foreach ($profile as $object)
                        {{ $object->agama }}
                      @endforeach
                      </td>
                      
                    </tr>
                  </tbody>
                </table>
              </div>
 
            </div>
    
@endsection

