<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Tag;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $post = Post::all();

        return view('cleanblog.home', compact('post'));
    }

    public function posts($id)
    {
        $post = Post::find($id);

        return view('cleanblog.post', compact('post'));
    }

    public function getCreatedAttribute(){
        return $this->attributes['created_at'];
    }

    public function home() {
        return view('cleanblog.erd');
    }

}
