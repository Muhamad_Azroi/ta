<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\User;

class ProfileController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
        $this->Profile = new Profile();
    }
    
    public function __invoke(Request $request)
    {
        //
    }

    public function index()
    {
        $data = [
            'profile' => $this->Profile->allData(),
        ];
        return view('profile', $data);
    } 

}
