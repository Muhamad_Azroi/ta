<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use App\Models\Tag;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use PDF;

//sweet alert
use RealRashid\SweetAlert\Facades\Alert;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = Auth::user();

        $posts = $user->posts; 
        //dd($posts);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $request->validate([
            'image' => 'required|image',
            'title' => 'required|unique:posts',
            'body' => 'required'
        ]);

        $image = $request->file('image');
        $fileName = time(). '.'. $image->getClientOriginalExtension();
        Storage::disk('local')->putFileAs('public/image', $image, $fileName);
        $tags_arr = explode(',', $request['tags']);
        $tag_ids = [];
        foreach($tags_arr as $tag_name) {
            $tag = Tag::firstOrCreate(['tag_name' => $tag_name]);
            $tag_ids[] = $tag->id;
        }

        $post = Post::create([
            "image" => $fileName,
            "title" => $request->title,
            "body" => $request->body,
            "user_id" => Auth::id()
        ]);
        $post->tags()->sync($tag_ids);
        $user = Auth::user();
        $user->posts()->save($post);
        
        Alert::success('Berhasil', 'Postingan Berhasil Ditambahkan');

        return redirect('/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        
        return view('posts.show', compact('post'));
    }

    public function download($id)
    {
        $post = Post::find($id);
        $pdf = PDF::loadView('posts.download', compact('post'));
        return $pdf->download('article.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);

        $update = Post::where('id', $id)->update([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);

        Alert::success('Berhasil', 'Postingan Berhasil Diedit');
        return redirect('/posts');    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);

        Alert::warning('Deleted', 'Postingan telah dihapus');
        return redirect('/posts'); 
    }
}
