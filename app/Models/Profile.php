<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Profile extends Model
{
    public function allData(){
        return DB::table('profile')
        ->leftJoin('users', 'users.id', '=', 'profile.user_id')
        ->get();        
    }
}
